import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

# Carregando o CSV
df = pd.read_csv("train.csv")
test_df = pd.read_csv("test.csv")

# Captando o target y
survived = df["Survived"]
y = survived.to_numpy()

# Captando as preditoras de interesse
columns = ["Sex", "Fare", "Pclass"]
X = df[columns]

# Convertendo variáveis categoricas
X["Sex"].replace(["male", "female"], [0, 1], inplace=True)
#X["Embarked"].replace(["Q", "S", "C"], [0, 1, 2], inplace=True)

# Resolvendo NaN
#X["Age"].fillna(X["Age"].mean(), inplace=True)
#X["Embarked"].fillna(0, inplace=True)


# Convertendo X para a forma de numpy array
X = X.to_numpy()


# Fazendo o split de treinamento e validação
X_train, X_validation, y_train, y_validation = train_test_split(
    X, y, test_size=0.2, random_state=42, shuffle=True)

# Treinando o modelo
clf = RandomForestClassifier(random_state = 42)
clf.fit(X_train, y_train)

# Acurária do modelo
print("A acurácia para o conjunto de validação é de ", clf.score(X_validation, y_validation))

# Fazendo a predição do conjunto teste
X_test = test_df[columns]
X_test["Sex"].replace(["male", "female"], [0, 1], inplace=True)
#X_test["Embarked"].replace(["Q", "S", "C"], [0, 1, 2], inplace=True)

# Tratando o caso de NaN
X_test["Fare"].fillna(X_test["Fare"].mean(), inplace=True)
#X_test["Age"].fillna(X_test["Age"].mean(), inplace=True)

X_test = X_test.to_numpy()   
print(X_test)                  
y_test = clf.predict(X_test)

print(y_test)

# Criando o csv da predição do conjunto teste
X_test_passengerId = test_df["PassengerId"].to_numpy()
test_dict = {"PassengerId": X_test_passengerId, "Survived":y_test}
df_test = pd.DataFrame.from_dict(test_dict)

# Index=False não cria a coluna da indexação
df_test.to_csv("result.csv", index=False)

print("Tudo certo")
