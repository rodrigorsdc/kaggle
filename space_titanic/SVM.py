import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import sys

df = pd.read_csv('train.csv') # carregando o dataset de treino
df_test = pd.read_csv('test.csv') # carregando o dataset de test

# captando o target y
transported = df["Transported"]
y = transported.to_numpy()

# Tratando a variável Cabin
deck = []
num = []
side = []
for s in df.loc[:, "Cabin"]:
    if (pd.isnull(s) == False):
        splited = s.split("/")
        deck.append(splited[0])
        num.append(splited[1])
        side.append(splited[2])
    else:
        deck.append(None)
        num.append(None)
        side.append(None)


df['Deck'] = deck
df['Num'] = num
df['Num'] = pd.to_numeric(df['Num'])
df['Side'] = side

group = []
inGroup = []
for s in df.loc[:, "PassengerId"]:
    s = s.split("_")
    group.append(int(s[0]))
    inGroup.append(int(s[1]))

df['Group'] = group
df['InGroup'] = inGroup

print("Group")
print(df['InGroup'].describe())

X = df.drop(columns=['Cabin', 'Transported', 'Name', 'PassengerId']) # Dropando colunas indesejadas
X_columns = X.columns

# Resolvendo variáveis categóricas
X['HomePlanet'], _= pd.factorize(X['HomePlanet'])
X['Destination'], _ = pd.factorize(X['Destination'])
X['VIP'], _ = pd.factorize(X['VIP'])
X['CryoSleep'], _ = pd.factorize(X['CryoSleep'])
X['Side'], _ = pd.factorize(X['Side'])
X['Deck'], _ = pd.factorize(X['Deck'])

print(X['Num'].describe())


# Resolvendo NA's
X['Age'].fillna(X['Age'].median(), inplace=True)
X['ShoppingMall'].fillna(X['Age'].median(), inplace=True)
X['Spa'].fillna(X['Spa'].median(), inplace=True)
X['VRDeck'].fillna(X['VRDeck'].median(), inplace=True)
X['FoodCourt'].fillna(X['FoodCourt'].median(), inplace=True)
X['RoomService'].fillna(X['RoomService'].median(), inplace=True)
X['Num'].fillna(X['Num'].mean(), inplace=True)
X['Side'].fillna(X['Side'].mode(), inplace=True)
X['Deck'].fillna(X['Deck'].mode(), inplace=True)

print(X['Deck'].isna().sum())
print(X['Side'].isna().sum())
print(X['Num'].isna().sum())

# Normalizando colunas
# X = (X-X.mean()) / X.std()

print(X['Deck'])

X = X.to_numpy() # Convertendo para a forma de numpy array

# Fazendo o split de trainamente e validação
# X_train, X_validation, y_train, y_validation = train_test_split(
#       X, y, test_size=0.2, random_state=42, shuffle=True)

# Treinando o modelo
clf = svm.SVC(kernerl='linear')
clf.fit(X, y)

# print("A acurácia para o conjunto de validação é de ", clf.score(X_validation, y_validation))

# Tratando a variável Cabin do test
deck = []
num = []
side = []
for s in df_test.loc[:, "Cabin"]:
    if (pd.isnull(s) == False):
        splited = s.split("/")
        deck.append(splited[0])
        num.append(splited[1])
        side.append(splited[2])
    else:
        deck.append(None)
        num.append(None)
        side.append(None)


df_test['Deck'] = deck
df_test['Num'] = num
df_test['Num'] = pd.to_numeric(df_test['Num'])
df_test['Side'] = side

group = []
inGroup = []
for s in df_test.loc[:, "PassengerId"]:
    s = s.split("_")
    group.append(int(s[0]))
    inGroup.append(int(s[1]))

df_test['Group'] = group
df_test['InGroup'] = inGroup

X_test = df_test[X_columns]

# Resolvendo variáveis categóricas do conjunto teste
X_test['HomePlanet'], _= pd.factorize(X_test['HomePlanet'])
X_test['Destination'], _ = pd.factorize(X_test['Destination'])
X_test['VIP'], _ = pd.factorize(X_test['VIP'])
X_test['CryoSleep'], _ = pd.factorize(X_test['CryoSleep'])
X_test['Side'], _ = pd.factorize(X_test['Side'])
X_test['Deck'], _ = pd.factorize(X_test['Deck'])

# Resolvendo NA's
X_test['Age'].fillna(X_test['Age'].median(), inplace=True)
X_test['ShoppingMall'].fillna(X_test['Age'].median(), inplace=True)
X_test['Spa'].fillna(X_test['Spa'].median(), inplace=True)
X_test['VRDeck'].fillna(X_test['VRDeck'].median(), inplace=True)
X_test['FoodCourt'].fillna(X_test['FoodCourt'].median(), inplace=True)
X_test['RoomService'].fillna(X_test['RoomService'].median(), inplace=True)
X_test['Num'].fillna(X_test['Num'].mean(), inplace=True)
X_test['Side'].fillna(X_test['Side'].mode(), inplace=True)
X_test['Deck'].fillna(X_test['Deck'].mode(), inplace=True)

# Normalizando
# X_test = (X_test - X_test.mean()) / X_test.std() 

X_test = X_test.to_numpy()
y_test = clf.predict(X_test)

# Criando o csv da predição
X_test_passengerId = df_test["PassengerId"].to_numpy()
test_dict = {"PassengerId": X_test_passengerId, "Transported":y_test}
df_test = pd.DataFrame.from_dict(test_dict)

df_test.to_csv("result.csv", index=False)

print("Tudo certo")
