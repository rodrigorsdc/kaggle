import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import sys

df_train = pd.read_csv('train.csv') # carregando o dataset de treino
df_test = pd.read_csv('test.csv') # carregando o dataset de test


df_train['train'] = 1
df_test['train'] = 0

df = pd.concat([df_train, df_test])

# captando o target y
transported = df_train["Transported"]
y = transported.to_numpy()

# Tratando a variável Cabin
deck = []
num = []
side = []
for s in df.loc[:, "Cabin"]:
    if (pd.isnull(s) == False):
        splited = s.split("/")
        deck.append(splited[0])
        num.append(splited[1])
        side.append(splited[2])
    else:
        deck.append(None)
        num.append(None)
        side.append(None)


df['Deck'] = deck
df['Num'] = num
df['Num'] = pd.to_numeric(df['Num'])
df['Side'] = side

# Tratando a variábel PassengerId
group = []
inGroup = []
for s in df.loc[:, "PassengerId"]:
    s = s.split("_")
    group.append(int(s[0]))
    inGroup.append(int(s[1]))

df['Group'] = group
df['InGroup'] = inGroup

# Captando o sobrenome dos passageiros
surname = []
for s in df.loc[:, "Name"]:
    if (pd.isnull(s) == False):
        s = s.split(" ")
        surname.append(s[1])
    else:
        surname.append("nan")

df['Surname'] = surname
X = df.drop(columns=['Cabin', 'Transported', 'Name', 'PassengerId', 'InGroup']) #Dropando colunas indesejadas
X_columns = X.columns


# Resolvendo variáveis categóricas
X['Surname'], _= pd.factorize(X['Surname'])
X['HomePlanet'], _= pd.factorize(X['HomePlanet'])
X['Destination'], _ = pd.factorize(X['Destination'])
X['VIP'], _ = pd.factorize(X['VIP'])
X['CryoSleep'], _ = pd.factorize(X['CryoSleep'])
X['Side'], _ = pd.factorize(X['Side'])
X['Deck'], _ = pd.factorize(X['Deck'])

# Resolvendo NA's
X['Age'].fillna(X['Age'].median(), inplace=True)
X['ShoppingMall'].fillna(X['ShoppingMall'].median(), inplace=True)
X['Spa'].fillna(X['Spa'].median(), inplace=True)
X['VRDeck'].fillna(X['VRDeck'].median(), inplace=True)
X['FoodCourt'].fillna(X['FoodCourt'].median(), inplace=True)
X['RoomService'].fillna(X['RoomService'].median(), inplace=True)
X['Num'].fillna(X['Num'].mean(), inplace=True)
X['Side'].fillna(X['Side'].mode(), inplace=True)
X['Deck'].fillna(X['Deck'].mode(), inplace=True)

# Normalizando as variáveis
X['Age'] = (X['Age'] - X['Age'].mean()) / X['Age'].std()
X['ShoppingMall'] = (X['ShoppingMall'] - X['ShoppingMall'].mean()) / X['ShoppingMall'].std()
X['VRDeck'] = (X['VRDeck'] - X['VRDeck'].mean()) / X['VRDeck'].std()
X['FoodCourt'] = (X['FoodCourt'] - X['FoodCourt'].mean()) / X['FoodCourt'].std()
X['RoomService'] = (X['RoomService'] - X['RoomService'].mean()) / X['RoomService'].std()

# Normalizando colunas
# X = (X-X.mean()) / X.std()

print(X['Num'])
print(X['Num'].describe())
print('Destination')

X_train = X[X['train'] == 1]
X_train = X_train.drop(columns=('train'))
X_test = X[X['train'] == 0]
X_test = X_test.drop(columns=('train'))

# Convertendo para a forma de numpy array
X_train = X_train.to_numpy()
X_test = X_test.to_numpy()

# Fazendo o split de trainamente e validação
# X_train, X_validation, y_train, y_validation = train_test_split(
#      X_train, y, test_size=0.2, random_state=42, shuffle=True)

model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(15,)),
    tf.keras.layers.Dense(5000, activation="relu"),
    tf.keras.layers.Dense(5000, activation="relu"),
    tf.keras.layers.Dense(2, activation="softmax")
    ])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(X_train, y, epochs=10)
y_test = model.predict(X_test)
y_test = np.argmax(y_test, axis=1)
y_test = np.bool_(y_test)

X_test_passengerId = df_test["PassengerId"].to_numpy()
test_dict = {"PassengerId": X_test_passengerId, "Transported":y_test}
df_test = pd.DataFrame.from_dict(test_dict)

df_test.to_csv("result.csv", index=False)

print("Tudo certo")
