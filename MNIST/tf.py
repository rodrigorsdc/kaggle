import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.keras import regularizers

df_train = pd.read_csv('train.csv')
df_test = pd.read_csv('test.csv')
y = df_train['label']
y = y.to_numpy()
X = df_train.drop(columns=('label'))
X = X.to_numpy()
X = X / 255
X = X.reshape(df_train.shape[0], 28, 28)

X_test = df_test.to_numpy()
X_test = X_test / 255
X_test = X_test.reshape(df_test.shape[0], 28, 28)

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(10, (3, 3), activation='relu', input_shape=(28, 28,1)),
#    tf.keras.layers.MaxPooling2D((2, 2)),
    tf.keras.layers.Conv2D(10, (3, 3), activation='relu'),
 #   tf.keras.layers.MaxPooling2D((2, 2)),
    tf.keras.layers.Conv2D(10, (3, 3), activation='relu'),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=regularizers.l2(0.001)),
#    tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=regularizers.l2(0.001)),
 #   tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=regularizers.l2(0.001)),
  #  tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Dense(10, activation='softmax'),
    ])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(X, y, epochs=15, verbose=1)

y_test = model.predict(X_test)
y_test = np.argmax(y_test, axis=1)

x_test_imageId = np.arange(1, y_test.shape[0] + 1)
test_dict = {"ImageId": x_test_imageId, "Label":y_test}
df_test = pd.DataFrame.from_dict(test_dict)

df_test.to_csv("result.csv", index=False)

print("Tudo certo")
